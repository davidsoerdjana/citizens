import React, { Component } from 'react'
import { Text, Image, Modal, ToastAndroid } from 'react-native'
import { Container, Content, Button, Item, Label, Input, Spinner, View , Header, Right, Left, Body } from 'native-base'
import { withNavigation } from 'react-navigation'
import Axios from 'axios'

class ForgotPassword extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            Modal: false
        }
    }

    forgot = async () => {
        this.setState({
            Modal: true
        })
        send = async (objParam) => await Axios.post(
            'https://app-citizenjournalism.herokuapp.com/api/v1/user/reset-password',
            objParam
        )

        send({
            email: this.state.email
        })
            .then(res => {
                this.setState({
                    Modal: false
                })
                this.props.navigation.navigate('Repassword', {email: this.state.email})
            })
            .catch(err => {
                this.setState({
                    Modal: false
                })
                ToastAndroid.show('Email Not Found', ToastAndroid.SHORT)
            })
    }

    render() {
        return (
            <Container
                style={{
                    flex: 1,
                    backgroundColor: '#4D4D4D'
                }}
            >
                <Header style=
                    {{
                    display: 'flex',
                    backgroundColor: '#4D4D4D'
                    }}
                >
                    <Left style={{flex: 1}}>
                        <Text style={{color: '#DFDFDF'}} onPress={() => this.props.navigation.goBack()}>Cancel</Text>
                    </Left>
                    <Body style={{
                            flex: 1
                        }}
                    >
                        <Image
                            source={require('../Logos/Logo.png')}
                            style={{
                                width: '100%',
                                height: '100%'
                            }}
                        />
                    </Body>
                    <Right style={{flex: 1}}>
                        <Text style={{color: '#4D4D4D'}}>Hho</Text>
                    </Right>
                </Header>
                <Content style={{alignSelf: "center", color: '#DFDFDF', marginVertical: 20, marginHorizontal: 10}}>
                    <Text style={{ fontFamily: 'Playfair', fontSize: 34, color: '#DFDFDF', marginVertical: 20}}>Reset Your Password</Text>
                    <Text style={{ fontSize: 15, color: "#DFDFDF" }}>
                        Input your e-mail to reset your password.
                    </Text>
                    <Text style={{ fontSize: 15, color: "#DFDFDF" }}>
                            We'll send instruction to reset your password to your e-mail.
                    </Text>
                        <Item stackedLabel style={{marginVertical: 20}}>
                        <Label style={{color: '#DFDFDF', fontSize: 15}}>E-mail</Label>
                        <Input 
                            style={{
                                color: 'white'
                            }}
                            onChangeText = { (text) => {
                                this.setState({
                                    email: text
                                })
                            }}
                        />
                    </Item>
                    <Button
                        onPress={this.forgot}
                        style={{
                            marginTop: 300,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            backgroundColor: '#3EA6FE',
                            width: 300,
                            height: 40,
                            borderRadius: 10
                        }}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#FFF',
                                fontWeight: 'bold'
                            }}
                        >
                            Reset My Password
                        </Text>
                    </Button>
                </Content>
                <Modal
                    animationType='fade'
                    transparent={true}
                    visible={this.state.Modal}
                    onRequestClose={
                        () => Alert.alert(`Success Login`)
                    }
                >
                    <View
                        style={{
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            display: 'flex',
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Spinner />
                    </View>
                </Modal>
            </Container>
        )
    }
}

export default withNavigation(ForgotPassword)