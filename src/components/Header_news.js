import React, { Component } from 'react'
import { Image } from 'react-native'
import { withNavigation } from 'react-navigation'
import { Header, Left, Icon, Body, Right, Text, Button } from 'native-base'
const photos = require('../Logos/Logo_Hitam.png')

class Header_about extends Component {
    render() {
        return (
            <Header
                style={{
                    display: 'flex',
                    backgroundColor: '#FFF'
                }}
            >
                <Left
                    style={{
                        flex: 1,
                        flexDirection: 'row',
                    }}
                >
                    <Button transparent
                        onPress={
                            () => {
                                this.props.navigation.goBack()
                            }
                        }
                    >
                        <Icon
                            type='Ionicons'
                            name='ios-arrow-back'
                            style={{ color: 'black' }}
                        />
                        <Text
                            style={{ margin: 5, alignSelf: 'center', color: 'black' }}
                        >
                            All News
                    </Text>
                    </Button>
                </Left>
                <Body
                    style={{
                        flex: 1
                    }}
                >
                    <Image
                        source={photos}
                        style={{
                            width: '100%',
                            height: '100%'
                        }}
                    />
                </Body>
                <Right
                    style={{
                        flex: 1
                    }}
                >
                    <Icon
                        type="Ionicons"
                        name="md-person"
                        style={{ margin: 5, color: 'white' }}

                    />
                </Right>
            </ Header>
        )
    }
}
export default withNavigation(Header_about)
