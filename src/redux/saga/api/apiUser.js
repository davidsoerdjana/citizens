import axios from 'axios';

export const apiGetUser = token => {
    return axios
        .get(
            'https://app-citizenjournalism.herokuapp.com/api/v1/user/profile',
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            }
        )
        .then(function (res) {
            return res.data.result;
        });
};