import { FETCH_TOKEN } from '../type/TokenType'

export const token = data => {
    return {
        type: FETCH_TOKEN,
        payload: data
    }
}