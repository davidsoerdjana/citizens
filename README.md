# Citizens

This Program is a product class's App of Glints Academy Challange. As for our graduation requirements, we are developing this app using React Native and the function of this app is to deliver a news for reader.

## Getting Started

Please follow these instructions to use and develop our project

### Prerequisites

For you whom want to use or develop this app, please install npm or yarn, Android SDK (Minimum 9.0 or Pie), JDK, and have an AVD or Android devices. 

### Installing

First you need to install all the requirements of my project

```
yarn install
```
or
```
npm install
```
and wait till the progress are completed

## Running the tes

### What will you see in the Program

The program will opened in your phone/AVD and showing a homepage

### And coding style tests

you can find the pages	:
1. Home		        : ```./src/Home.js```
2. Login	        : ```./src/Login.js```
3. Register	        : ```./src/Register.js```
4. Forgot Pass      : ```./src/ForgotPassword.js```
5. News             : ```./src/News.js```
6. Profile          : ```./src/Profile.js```
7. Upload           : ```./src/Upload.js```
8. Contributor      : ```./src/Contributor.js```
9. AboutUs          : ```./src/AboutUs.js```
10. Edit Profile    : ```./src/Edit.js```
11. Verification    : ```./src/Verification.js```
12. Category        : ```./src/Category.js```
13. Search          : ```./src/search.js```

## Deployment

After you installing the requirements of my projects
open a Terminal and type
```
npm start
```
or
```
yarn start
```
or
```
react-native start
```
to open the services, and open new terminal windows and type this command
```
react-native run-android
```

and wait till the process are completed

## Built With

* [React-Native](https://facebook.github.io/react-native/) - The Mobile App Library
* [Express JS](https://expressjs.com/) - Back-end API controller
* [Native Base](https://nativebase.io/)- React-Native Library for styling

## Contributing

* [Khairunnisha Aaliyy Afifa](https://gitlab.com/khairunnishaafifa) - BackEnd

* [Angelia Purnamasari](https://gitlab.com/angellee177) - BackEnd

* [Lim Kaleb Octavianus](https://gitlab.com/limkaleb) - BackEnd

* [Joe Phang Rahmansyah](https://gitlab.com/han07) - React-Native

* [Aldo Lim](https://gitlab.com/aldoxyn) - React Native

* [Agung Dwi Putra](https://gitlab.com/agungdp50) - FrontEnd

## Versioning

0.1 : initial Release

## Authors

* **Joe Phang Rahmansyah** - *Final Product* - [Han07](https://gitlab.com/han07)
* **Aldo Lim** - *Final Product* - [Aldoxyn](https://gitlab.com/aldoxyn)