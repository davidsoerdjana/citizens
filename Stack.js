import React from 'react';
import Home from './src/pages/Home'
import Login from './src/pages/Login'
import Profile from './src/pages/Profile'
import Register from './src/pages/Register'
import ForgotPassword from './src/pages/ForgotPassword'
import News from './src/pages/News'
import AboutUs from './src/pages/AboutUs'
import Contributor from './src/pages/Contributor'
import Category from './src/pages/Category'
import Verification from './src/pages/Verification'
import Upload from './src/pages/Upload'
import Edit from './src/pages/Edit'
import Repassword from './src/pages/Repassword'
import ProfileDetail from './src/pages/ProfileDetail'
import search from './src/pages/search'
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { Icon } from 'native-base'
import CategoryPage from './src/pages/CategoryPage';
import News_List from './src/pages/News_List';

const StackNav = createStackNavigator(
    {
        Home: {
            screen: Home
        },
        Login: {
            screen: Login
        },
        Profile: {
            screen: Profile
        },
        Register: {
            screen: Register
        },
        ForgotPassword: {
            screen: ForgotPassword
        },
        AboutUs: {
            screen: AboutUs
        },
        News: {
            screen: News
        },
        Upload: {
            screen: Upload
        },
        Contributor: {
            screen: Contributor
        },
        Category: {
            screen: Category
        },
        Verification: {
            screen: Verification
        },
        Edit: {
            screen: Edit
        },
        Repassword: {
            screen: Repassword
        },
        ProfileDetail: {
            screen: ProfileDetail
        },
        Search: {
            screen: search
        }
    },
    {
        initialRouteName: 'Home',
        headerMode: 'none'
    }
)

const HomeStack = createStackNavigator(
    {
        Home: { screen: Home },
        News: { screen: News },
        Search: { screen: search },
        ProfileDetail: { screen: ProfileDetail },
        Category: { screen: Category },
        Login: { screen: Login },
        CategoryPage: { screen: CategoryPage }
    },
    {
        initialRouteKey: 'Home',
        headerMode: 'none'
    }
)

const LoginStack = createStackNavigator(
    {
        Profile: { screen: Profile },
        Login: { screen: Login },
        Register: { screen: Register },
        ForgotPassword: { screen: ForgotPassword },
        Verification: { screen: Verification },
        Repassword: { screen: Repassword },
        Edit: { screen: Edit },
        AboutUs: { screen: AboutUs },
        News_list: { screen: News_List }
    },
    {
        initialRouteKey: 'Profile',
        headerMode: 'none'
    }
)

const ContributorStack = createStackNavigator(
    {
        Contributor: { screen: Contributor },
        Profile: { screen: ProfileDetail },
        News: { screen: News },
        Login: { screen: Login }
    },
    {
        initialRouteKey: 'Contributor',
        headerMode: 'none'
    }
)

const BottomNav = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: {
                title: 'Home',
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        type='Ionicons'
                        name='md-home'
                        style={{
                            color: tintColor
                        }}
                    />
                )
            }
        },
        Contributor: {
            screen: ContributorStack,
            navigationOptions: {
                title: 'Contributor',
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        type='Ionicons'
                        name='contacts'
                        style={{
                            color: tintColor
                        }}
                    />
                )
            }
        },
        AddNews: {
            screen: Upload,
            navigationOptions: {
                title: 'Upload',
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        type='Ionicons'
                        name='md-add'
                        style={{
                            color: tintColor
                        }}
                    />
                )
            }
        },
        Profile: {
            screen: LoginStack,
            navigationOptions: {
                title: 'Profile',
                tabBarIcon: ({ tintColor }) => (
                    <Icon
                        type='Ionicons'
                        name='md-person'
                        style={{
                            color: tintColor
                        }}
                    />
                )
            }
        }
    },
    {
        initialRouteKey: 'Home',
        headerMode: 'none',
        tabBarOptions: {
            activeTintColor: '#3EA6FE',
            inactiveTintColor: '#333333',
            activeBackgroundColor: '#FFF',
            inactiveBackgroundColor: '#FFF'
        }
    }
)

export default AppContainer = createAppContainer(BottomNav)